init: setup delete_routes conf_routes

setup:
	docker-compose up -d

clean:
	docker-compose down;

delete_routes:
	docker exec -ti b1 ip route del default;
	docker exec -ti r2 ip route del default;
	docker exec -ti r3 ip route del default;
	docker exec -ti b4 ip route del default;
	docker exec -ti b5 ip route del default;
	docker exec -ti b1 ip -6 route del default;
	docker exec -ti r2 ip -6 route del default;
	docker exec -ti r3 ip -6 route del default;
	docker exec -ti b4 ip -6 route del default;
	docker exec -ti b5 ip -6 route del default;
	docker exec -ti h1 ip route del default via 192.168.9.1;
	docker exec -ti h2 ip route del default via 192.168.8.1;
	docker exec -ti h3 ip route del default via 192.168.7.1;
	docker exec -ti web ip route del default via 192.168.0.1;
	docker exec -ti mail ip route del default via 192.168.0.1;
	docker exec -ti h1 ip -6 route del default via 2000:9::1;
	docker exec -ti h2 ip -6 route del default via 2000:8::1;
	docker exec -ti h3 ip -6 route del default via 2000:7::1;
	docker exec -ti web ip -6 route del default via 2000::1;
	docker exec -ti mail ip -6 route del default via 2000::1;

conf_routes:
	docker exec -ti h1 ip route add default via 192.168.9.10;
	docker exec -ti h2 ip route add default via 192.168.8.10;
	docker exec -ti h3 ip route add default via 192.168.7.10;
	docker exec -ti web ip route add default via 192.168.0.10;
	docker exec -ti mail ip route add default via 192.168.0.10;
	docker exec -ti h1 ip -6 route add default via 2000:9::10;
	docker exec -ti h2 ip -6 route add default via 2000:8::10;
	docker exec -ti h3 ip -6 route add default via 2000:7::10;
	docker exec -ti web ip -6 route add default via 2000::10;
	docker exec -ti mail ip -6 route add default via 2000::10;

check_connectivity:
	docker exec -ti h1 ping -c 3 192.168.0.11;
	docker exec -ti h1 ping -c 3 192.168.0.12;
	docker exec -ti h2 ping -c 3 192.168.0.11;
	docker exec -ti h2 ping -c 3 192.168.0.12;
	docker exec -ti h3 ping -c 3 192.168.0.11;
	docker exec -ti h3 ping -c 3 192.168.0.12;
	docker exec -ti h1 ping6 -c 3 2000::11;
	docker exec -ti h1 ping6 -c 3 2000::12;
	docker exec -ti h2 ping6 -c 3 2000::11;
	docker exec -ti h2 ping6 -c 3 2000::12;
	docker exec -ti h3 ping6 -c 3 2000::11;
	docker exec -ti h3 ping6 -c 3 2000::12;

check_webserver:
	docker exec -ti h1 curl -4 http://192.168.0.11;
	docker exec -ti h2 curl -4 http://192.168.0.11;
	docker exec -ti h3 curl -4 http://192.168.0.11;
	docker exec -ti h1 curl -g -6 http://[2000::11];
	docker exec -ti h2 curl -g -6 http://[2000::11];
	docker exec -ti h3 curl -g -6 http://[2000::11];

send_mail:
	docker exec -ti h1 swaks --to lmonsierra@gmail.com --server [2000::12]:1025 --body "Mail de prueba" --header "Subject: TP3 BGP"